package it.com.atlassian.confluence.plugins.livesearch;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.search.SearchPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.collect.Lists;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchMacro;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchSearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static java.lang.String.format;
import static java.util.stream.StreamSupport.stream;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(ConfluenceStatelessTestRunner.class)
public class LiveSearchMacroTestCase {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRpcClient rpcClient;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static SpaceFixture anotherSpace = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .space(space)
            .author(user)
            .title("LiveSearch Test")
            .content("{livesearch}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture anotherPage = pageFixture()
            .space(anotherSpace)
            .author(user)
            .title("Another Test Page")
            .build();

    private LiveSearchMacro getLiveSearchMacro(final Content content) {
        product.loginAndView(user.get(), content);
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
        return product.getPageBinder().bind(LiveSearchMacro.class);
    }

    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testPressingButtonPerformsGlobalSearch() {
        final LiveSearchMacro macro = getLiveSearchMacro(page.get());
        final SearchPage searchPage = macro.clickSearch();
        assertNotNull(searchPage);
    }

    //CONFDEV-19661
    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testDropDownShouldAppearNoResultRecordWhenSearchLengthEqual3() {
        final LiveSearchMacro macro = getLiveSearchMacro(page.get());

        final String searchText = "aaa";
        final Iterator<LiveSearchSearchResult> resultItr = macro.searchFor(searchText).iterator();
        assertTrue(resultItr.hasNext());

        final LiveSearchSearchResult result = resultItr.next();
        assertEquals("No results found for '" + searchText + "'", result.getTitle());
        assertFalse(resultItr.hasNext());
    }

    // CONFDEV-19661
    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testDropDownShouldNotAppearWhenSearchLengthEqual2() {
        final LiveSearchMacro macro = getLiveSearchMacro(page.get());

        final String searchText = "aa";
        final Iterator<LiveSearchSearchResult> resultItr = macro.searchFor(searchText).iterator();
        assertFalse(resultItr.hasNext());
        assertFalse(macro.isDropDownVisible());
    }

    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testPressingButtonPerformsSpaceSpecificSearchWhenSpaceKeyParamSpecified() {
        final Content pageInSpace = restClient.getAdminSession().contentService().create(
                Content.builder()
                        .space(space.get())
                        .title("LiveSearch Macro Test")
                        .type(ContentType.PAGE)
                        .body(format("{livesearch:spaceKey=%s}", space.get().getKey()), ContentRepresentation.WIKI)
                        .build()
        );
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        LiveSearchMacro macro = getLiveSearchMacro(pageInSpace);

        macro.searchFor("test");

        SearchPage searchPage = macro.clickSearch();
        List<PageElement> results = searchPage.results();

        for (PageElement result : results) {
            assertThat(result.getText(), not(containsString(anotherPage.get().getTitle())));
        }
    }

    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testLiveSearchWithSpaceScope() {
        final Content pageInSpace = restClient.getAdminSession().contentService().create(
                Content.builder()
                        .space(space.get())
                        .title("LiveSearch Macro Test")
                        .type(ContentType.PAGE)
                        .body(format("{livesearch:spaceKey=%s}", space.get().getKey()), ContentRepresentation.WIKI)
                        .build()
        );
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        final LiveSearchMacro macro = getLiveSearchMacro(pageInSpace);

        final Iterable<LiveSearchSearchResult> results = macro.searchFor("test");

        Predicate<LiveSearchSearchResult> additionalPredicate = LiveSearchMacroTestUtil.searchResultByAdditional(space.get().getName());
        Predicate<LiveSearchSearchResult> contentTypePredicate = LiveSearchMacroTestUtil.searchResultByContentType("spacedesc");
        Predicate<LiveSearchSearchResult> spaceUrlPredicate = LiveSearchMacroTestUtil.searchResultByUrl(format("%s/display/%s", product.getProductInstance().getBaseUrl(), space.get().getKey()));

        // non space
        final Iterable<LiveSearchSearchResult> nonSpaceFiltered = stream(results.spliterator(), false)
                .filter(additionalPredicate.and(contentTypePredicate.negate()))
                .collect(Collectors.toList());

        // space
        final Iterable<LiveSearchSearchResult> spaceFiltered = stream(results.spliterator(), false)
                .filter(spaceUrlPredicate.and(contentTypePredicate))
                .collect(Collectors.toList());

        List<LiveSearchSearchResult> resultList = Lists.newArrayList(results);
        List<LiveSearchSearchResult> nonSpaceList = Lists.newArrayList(nonSpaceFiltered);
        List<LiveSearchSearchResult> spaceList = Lists.newArrayList(spaceFiltered);

        assertEquals(resultList.size(), nonSpaceList.size() + spaceList.size());

    }

    // CONF-7496
    @Test
    @ResetFixtures(value = {"space", "page"}, when = ResetFixtures.When.BEFORE)
    public void testStemming() {
        final Content pageInSpace = restClient.getAdminSession().contentService().create(
                Content.builder()
                        .space(space.get())
                        .title("LiveSearch Macro Test")
                        .type(ContentType.PAGE)
                        .body(format("{livesearch:spaceKey=%s}", space.get().getKey()), ContentRepresentation.WIKI)
                        .build()
        );
        final Content eclipsePage = restClient.getAdminSession().contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("page title 1")
                        .body("eclipse", ContentRepresentation.STORAGE)
                        .build()
        );
        final Content eclipseAndStuffPage = restClient.getAdminSession().contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("page title 2")
                        .body("eclipse and stuff", ContentRepresentation.STORAGE)
                        .build()
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        final LiveSearchMacro macro = getLiveSearchMacro(pageInSpace);

        final Iterable<LiveSearchSearchResult> results = macro.searchFor("eclipse* OR eclipse");

        final Predicate<LiveSearchSearchResult> resultsByEclipseTitle = LiveSearchMacroTestUtil.searchResultByTitle(eclipsePage.getTitle());
        final Predicate<LiveSearchSearchResult> resultsByEclipseAndStuffTitle = LiveSearchMacroTestUtil.searchResultByTitle(eclipseAndStuffPage.getTitle());


        final Iterable<LiveSearchSearchResult> filtered = stream(results.spliterator(), false)
                .filter(resultsByEclipseTitle.or(resultsByEclipseAndStuffTitle))
                .collect(Collectors.toList());

        final List<LiveSearchSearchResult> resultList = Lists.newArrayList(results);
        final List<LiveSearchSearchResult> filteredList = Lists.newArrayList(filtered);

        assertThat(resultList, hasSize(2));
        assertThat(filteredList, hasSize(2));
    }
}
