package it.com.atlassian.confluence.plugins.livesearch;

import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchSearchResult;

import javax.annotation.Nullable;
import java.util.function.Predicate;

public final class LiveSearchMacroTestUtil {
    private LiveSearchMacroTestUtil() {
    }

    private static class LiveSearchSearchResultByUrl implements Predicate<LiveSearchSearchResult> {
        private final String url;

        public LiveSearchSearchResultByUrl(String url) {
            this.url = url;
        }

        @Override
        public boolean test(@Nullable LiveSearchSearchResult result) {
            return result.getUrl().contains(url);
        }
    }

    private static class LiveSearchSearchResultByContentType implements Predicate<LiveSearchSearchResult> {
        private final String contentType;

        public LiveSearchSearchResultByContentType(String contentType) {
            this.contentType = contentType;
        }

        @Override
        public boolean test(@Nullable LiveSearchSearchResult result) {
            return result.getContentType().equals(contentType);
        }
    }

    private static class LiveSearchSearchResultByAdditional implements Predicate<LiveSearchSearchResult> {
        private final String additional;

        public LiveSearchSearchResultByAdditional(String additional) {
            this.additional = additional;
        }

        @Override
        public boolean test(@Nullable LiveSearchSearchResult result) {
            return (null != result.getAdditional()) && result.getAdditional().equals(additional);
        }
    }

    private static class LiveSearchSearchResultByTitle implements Predicate<LiveSearchSearchResult> {
        private final String title;

        public LiveSearchSearchResultByTitle(String title) {
            this.title = title;
        }

        @Override
        public boolean test(@Nullable LiveSearchSearchResult result) {
            return (result.getTitle().equals(title));
        }
    }

    public static Predicate<LiveSearchSearchResult> searchResultByUrl(String url) {
        return new LiveSearchSearchResultByUrl(url);
    }

    public static Predicate<LiveSearchSearchResult> searchResultByContentType(String type) {
        return new LiveSearchSearchResultByContentType(type);
    }

    public static Predicate<LiveSearchSearchResult> searchResultByAdditional(String additional) {
        return new LiveSearchSearchResultByAdditional(additional);
    }

    public static Predicate<LiveSearchSearchResult> searchResultByTitle(String title) {
        return new LiveSearchSearchResultByTitle(title);
    }
}
